from flask import Flask, render_template, url_for, request, session
import sqlite3 as sql
import sqlite3
from flask_mail import Mail, Message
app = Flask(__name__)

# conn = sqlite3.connect('database.db')
# conn.execute('CREATE TABLE applications (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, username TEXT, phone_number TEXT, selecting TEXT)')
# conn.close()
app.config['SECRET_KEY'] = '6d4be3c6e72e8fd1baa90681b6f6295f85502af4'
app.config['MAIL_SERVER'] = 'smtp.googlemail.com'
app.config['MAIL_PORT'] = 465
app.config['MAIL_USERNAME'] = 'vuryses2001@gmail.com'
app.config['MAIL_DEFAULT_SENDER'] = 'vuryses2001@gmail.com'
app.config['MAIL_PASSWORD'] = 'zwfipefizzughipb'
app.config['MAIL_USE_TLS'] = False
app.config['MAIL_USE_SSL'] = True
mail = Mail(app)


@app.route("/")
def index():
    return render_template("index.html")

@app.route("/contact/")
def contact():
    message = "Заполните форму, чтобы записаться к нашему мастеру"
    if 'visits' in session:
        session['visits'] = session.get('visits') + 1
    else:
        session['visits'] = 1
    return render_template("contact.html", message = message)

@app.route('/address',methods = ['POST', 'GET'])
def address():
    if request.method == 'POST':
        try:
            session['username'] = request.form['username']
            username = request.form['username']
            phone_number = request.form['phone_number']
            selecting = request.form['selecting']
            msg = Message('Get-Quote Form', sender = 'vuryses2001@gmail.com', recipients = ['vuryses2001@gmail.com'])
            msg.body = 'Фамилия и имя клиента: {} \nНомер телефона: {} \nПроцедура: {}'.format(session['username'],phone_number,selecting)
            mail.send(msg)
            with sql.connect("database.db") as con:
                cur = con.cursor()
                cur.execute("""INSERT INTO applications (username,phone_number,selecting)
                VALUES (?,?,?)""", (username, phone_number, selecting) )
                con.commit()
        finally:
            con = sql.connect("database.db")
            con.row_factory = sql.Row
            cur = con.cursor()
            cur.execute("""SELECT * FROM applications WHERE ID = (SELECT MAX(ID) FROM applications)""")
            rows = cur.fetchall();
            return render_template("list.html",rows = rows)
            con.close()

if __name__ == "__main__":
    app.run()
